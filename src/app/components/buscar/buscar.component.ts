import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { HeroesService, Heroe } from '../../services/heroes.service';
@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
})
export class BuscarComponent implements OnInit {

  heroes:Heroe[] = [];
  termino:string = "0";

  constructor(  private activatedRoute:ActivatedRoute,
                private _heroesService: HeroesService,
              private router:Router) {

    this.activatedRoute.params.subscribe( params => {
      this.heroes = this._heroesService.buscarHeroe(params['name']);
      this.termino = params['name'];
      console.log(this.heroes);
    })

  }
  ngOnInit() {
  }


  verHeroe(idx:number){
    this.router.navigate(['/heroe', idx]);
  }
}
