import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styleUrls: ['./heroe-tarjeta.component.css']
})
export class HeroeTarjetaComponent implements OnInit {
  @Input() heroe:any = {};
  @Input() heroeId:number;

  @Output() selectedHeroe: EventEmitter<number>;

  constructor(private router:Router) {
    this.selectedHeroe = new EventEmitter();
  }

  ngOnInit() {
  }

  verHeroe(){

    // this.router.navigate(['/heroe', this.heroe.idx]);
    this.selectedHeroe.emit(this.heroeId);
  }

}
